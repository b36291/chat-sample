export default {
  install: (app, options) => {
    app.directive("msg", {
      mounted(el, binding, vnode, oldVnode) {
        let isUrl = false;
        let isImg = false;
        const checkType = () => {
          const picReg = new RegExp(/\.(png|jpe?g|gif|svg)(\?.*)?$/);
          const urlExp = new RegExp(
            /http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/
          );
          isUrl = urlExp.test(binding.value);
          isImg = picReg.test(binding.value);
        };

        const linkURL = () => {
          const arr = binding.value.split("http");
          const url = "http" + arr[1];

          el.innerHTML = `${arr[0]}<br><a href="${url}" target="blank_" style="color: gray">${url}</a>`;
        };

        const linkIMG = () => {
          const arr = binding.value.split("http");
          const url = "http" + arr[1];

          el.innerHTML = `${arr[0]}<br><img style="max-width: 100%" src="${url}" />`;
          el.onclick = function () {
            window.open(url, "blank_");
          };
        };

        checkType();

        if (isImg) {
          linkIMG();
        } else if (isUrl) {
          linkURL();
        } else {
          el.innerHTML = binding.value;
        }
      }
    });
  }
};
